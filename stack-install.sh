#!/bin/bash

git clone https://github.com/dhilditch/wpintense-rocket-stack-ubuntu18-wordpress &&
cp wpintense-rocket-stack-ubuntu18-wordpress/nginx/* /etc/nginx/ -R &&
ln -s /etc/nginx/sites-available/rocketstack.conf /etc/nginx/sites-enabled/ &&
rm /etc/nginx/sites-enabled/default &&
mkdir /var/www/cache &&
mkdir /var/www/cache/rocketstack &&
chown www-data:www-data /var/www/cache/ -R
