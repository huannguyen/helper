#!/bin/bash

wget https://wordpress.org/latest.zip -P /var/www/ &&
unzip /var/www/latest.zip -d /var/www/ &&
mv /var/www/wordpress /var/www/rocketstack &&
chown www-data:www-data /var/www/rocketstack -R &&
rm /var/www/latest.zip
